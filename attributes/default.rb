# The PidFile option specifies the name of a file to which the process id (PID) will be written. This is overridden by the −i command line option. There is no default value for the PidFile option.
default['radsecproxy']['general']['pidfile'] = nil

# This option specifies the debug level. It must be set to 1, 2, 3, 4 or 5, where 1 logs only serious errors, and 5 logs everything. The default is 2 which logs errors, warnings and a few informational messages. Note that the command line option −d overrides this.
default['radsecproxy']['general']['loglevel'] = nil

# This specifies where the log messages should go. By default the messages go to syslog with facility LOG_DAEMON. Using this option you can specify another syslog facility, or you may specify that logging should be to a particular file, not using syslog. The value must be either a file or syslog URL. The file URL is the standard one file:///var/log/radius.log, specifying a local file that should be used. For syslog, you must use the syntax: x−syslog:///FACILITY where FACILITY must be one of LOG_DAEMON, LOG_MAIL, LOG_USER, LOG_LOCAL0, LOG_LOCAL1, LOG_LOCAL2, LOG_LOCAL3, LOG_LOCAL4, LOG_LOCAL5, LOG_LOCAL6or LOG_LOCAL7. You may omit the facility from the URL to specify logging to the default facility, but this is not very useful since this is the default log destination. Note that this option is ignored if −f is specified on the command line.
default['radsecproxy']['general']['logdestination'] = nil

# This can be set to on to include the thread-id in the log messages (useful for debugging).
default['radsecproxy']['general']['logthreadid'] = nil

# This can be set to off to only log the realm in Access-Accept/Reject log messages (for privacy).
default['radsecproxy']['general']['logfullusername'] = nil

# The LogMAC option can be used to control if and how Calling-Station-Id (the users Ethernet MAC address) is being logged. It can be set to one of Static, Original, VendorHashed, VendorKeyHashed, FullyHashed or FullyKeyHashed. The default value for LogMAC is Original.
# See radsecproxy.conf−example for details.
default['radsecproxy']['general']['logmac'] = nil

# The LogKey option is used to specify the key to use when producing HMAC’s as an effect of specifying VendorKeyHashed or FullyKeyHashed for the LogMAC option.
default['radsecproxy']['general']['logkey'] = nil

# The FTicksReporting option is used to enable F-Ticks logging and can be set to None, Basic or Full. Its default value is None. If FTicksReporting is set to anything other than None, note that the default value for FTicksMAC needs FTicksKey to be set.
# See radsecproxy.conf−example for details.
default['radsecproxy']['general']['fticksreporting'] = nil

# The FTicksMAC option has the same function as LogMAC for FTicks. The default for FTicksMAC is VendorKeyHashed which needs FTicksKey to be set.
# Before choosing any of Original, FullyHashed or VendorHashed, consider the implications for user privacy when MAC addresses are collected. How will the logs be stored, transferred and accessed?
default['radsecproxy']['general']['fticksmac'] = nil

# The FTicksKey option has the same function as LogKey for Fticks.
default['radsecproxy']['general']['ftickskey'] = nil

# The FTicksSyslogFacility option is used to specify a dedicated syslog facility for F-Ticks messages. This allows for easier filtering of F-Ticks messages. If no FTicksSyslogFacility option is given, F-Ticks messages are written to what the LogDestination option specifies.
# F-Ticks messages are always logged using the log level LOG_DEBUG. Note that specifying a file in FTicksSyslogFacility (using the file:/// prefix) is not supported.
default['radsecproxy']['general']['ftickssyslogfacility'] = nil

# The FTicksPrefix option is used to set the prefix printed in F-Ticks messages. This allows for use of F-Ticks messages in non-eduroam environments. If no FTicksPrefix option is given, it defaults to the prefix used for eduroam (F−TICKS/eduroam/1.0).
default['radsecproxy']['general']['fticksprefix'] = nil

# Listen for the address and port for the respective protocol. Normally the proxy will listen to the standard ports if configured to handle clients with the respective protocol. The default ports are 1812 for UDP and TCP and 2083 for TLS and DTLS. On most systems it will do this for all of the system’s IP addresses (both IPv4 and IPv6). On some systems however, it may respond to only IPv4 or only IPv6. To specify an alternate port you may use a value on the form *:port where port is any valid port number. If you also want to specify a specific address you can do e.g. 192.168.1.1:1812 or [2001:db8::1]:1812. The port may be omitted if you want the default one. Note that you must use brackets around the IPv6 address. These options may be specified multiple times to listen to multiple addresses and/or ports for each protocol.
default['radsecproxy']['general']['listenudp'] = nil
default['radsecproxy']['general']['listentcp'] = nil
default['radsecproxy']['general']['listentls'] = nil
default['radsecproxy']['general']['listendtls'] = nil

# This can be used to specify source address and/or source port that the proxy will use for connecting to clients to send messages (e.g. Access Request). The same syntax as for Listen... applies.
default['radsecproxy']['general']['sourceudp'] = nil
default['radsecproxy']['general']['sourcetcp'] = nil
default['radsecproxy']['general']['sourcetls'] = nil
default['radsecproxy']['general']['sourcedtls'] = nil

# This can be used to change the default TTL attribute. Only change this if you know what you are doing. The syntax is either a numerical value denoting the TTL attribute, or two numerical values separated by column specifying a vendor attribute.
default['radsecproxy']['general']['ttlattribute'] = nil

# If a TTL attribute is present, the proxy will decrement the value and discard the message if zero. Normally the proxy does nothing if no TTL attribute is present. If you use the AddTTL option with a value 1-255, the proxy will, when forwarding a message with no TTL attribute, add one with the specified value. Note that this option can also be specified for a client/server which will override this setting when forwarding a message to that client/server.
default['radsecproxy']['general']['addttl'] = nil

# When this is enabled (on), a request will never be sent to a server named the same as the client it was received from. I.e., the names of the client block and the server block are compared. Note that this only gives limited protection against loops. It can be used as a basic option and inside server blocks where it overrides the basic setting.
default['radsecproxy']['general']['loopprevention'] = nil

# Enabling IPv4Only or IPv6Only (on) makes radsecproxy resolve DNS names to the corresponding address family only, and not the other. This is done for both clients and servers. At most one of IPv4Only and IPv6Only can be enabled. Note that this can be overridden in client and server blocks, see below.
default['radsecproxy']['general']['ipv4only'] = nil
default['radsecproxy']['general']['ipv6only'] = nil

# This is not a normal configuration option; it can be specified multiple times. It can both be used as a basic option and inside blocks. For the full description, see the configuration syntax section above.
default['radsecproxy']['include'] = []

default['radsecproxy']['clients'] = {
  '127.0.0.1' => {
    'type' => 'udp',
    'secret' => 'testing123',
  },
}

default['radsecproxy']['servers'] = {}

default['radsecproxy']['realms'] = {
  '*' => {
    'replymessage' => 'User unknown',
  },
}
