name             'radsecproxy'
maintainer       'HPC'
maintainer_email 'hpc@gsi.de'
license          'Apache-2.0'
description      'Installs/Configures radsecproxy'
version          '1.1.0'
source_url       'https://git.gsi.de/chef/cookbooks/radsecproxy'
issues_url       'https://git.gsi.de/chef/cookbooks/radsecproxy/issues'
