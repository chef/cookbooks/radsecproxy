#
# Cookbook:: radsecproxy
# Recipe:: default
#
# Copyright:: 2020-2023 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  André Kerkhoff   <a.kerkhoff@gsi.de>
#  Christopher Huhn <c.huhn@gsi.de>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

Chef::Resource::Template.include Radsecproxy::Helper

package 'radsecproxy'

service 'radsecproxy' do
  action %i(start enable)
end

# Debian Buster runs the service as a dedicated user 'radsecproxy'
#  older versions run it as 'root'
radsecproxy_user = if platform?('debian') &&
                      node['platform_version'].to_i >= 10
                     'radsecproxy'
                   else
                     'root'
                   end
# the user has a dedicated group of the same name:
radsecproxy_group = radsecproxy_user

if radsecproxy_user.eql?('radsecproxy') && node.exist?('etc', 'group', 'ssl-cert')
  execute 'usermod -a -G ssl-cert radsecproxy' do
    not_if { node['etc']['group']['ssl-cert']['members'].include? 'radsecproxy' }
  end
end

template '/etc/radsecproxy.conf' do
  source 'radsecproxy.conf.erb'
  variables radsecproxy_retrieve_secrets(node['radsecproxy'])
  user 'root'
  group radsecproxy_group
  mode '0640'
  notifies :restart, 'service[radsecproxy]'
end
