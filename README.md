# radsecproxy

This cookbook installs [radsecproxy](https://radsecproxy.github.io/). All attributes correspond to those in the radsecproxy configuration file.

## Attributes

Attributes are ordered by sections. Values can be boolean values (`true/false`) which will be translated to `on/off`.

```ruby
node['radsecproxy'] = {
  general: {
    # general options
	loglevel: 5,
  }
  include: [], # Include files
  clients: {
    # A map from client names to client options
    client_name: {
      type: 'tls',
    }
  },
  servers: {
    # A map from server names to server options
    server_name: {
      type: 'tls',
    }
  },
  realms: {
    # A map from realm names to realm options
    realm_name: {
      replymessage: 'User unknown',
      # Special realm options
      servers: %w(server_name),
      accounting_servers: %w(server_name),
    }
  }
}
```

## Secrets

In every attribute all occurences of format strings like `{vault:<vault>:<item>:<value>}` will be replaced with the corresponding Chef Vault values.

## Recipes

There is only one default recipe which installs and configures radsecproxy.
