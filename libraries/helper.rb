# Cookbook:: radsecproxy
# Library:: helper
#
# Copyright:: 2020, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module Radsecproxy
  # Helper functions for the FreeRADIUS cookbook
  module Helper
    module_function

    # Retrieve all secrets in an attributes hash from Chef Vault
    def radsecproxy_retrieve_secrets(attributes)
      attributes = attributes.to_h if attributes.is_a? Chef::Node::ImmutableMash
      attributes.each do |key, value|
        if value.is_a?(Chef::Node::ImmutableMash) || value.is_a?(Hash)
          attributes[key] = radsecproxy_retrieve_secrets(value)
          next
        end
        next unless value.is_a? String
        next unless value =~ /{vault:[^:]+:[^:]+:[^:]+}/

        require 'chef-vault'
        attributes[key] = value.sub(/{vault:[^:]+:[^:]+:[^:]+}/) do |secret|
          values = secret[1..-2].split(':')
          vault = ChefVault::Item.load(values[1], values[2])
          vault.nil? ? nil : vault[values[3]]
        end
      end
      attributes
    end
  end
end
